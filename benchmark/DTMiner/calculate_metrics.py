import pandas as pd
import subprocess

if __name__ == "__main__":

    candidate_gdas = pd.read_csv("out/candidate_GDAs.csv", names=['pmid', 'diseaseId', 'geneId'], sep="\t")
    
    predict = pd.read_csv("out/DTMiner_predict", names=['predict'])
    
    candidate_gdas['predict'] = predict.predict
    
    candidate_gdas.to_csv("out/classification_result_dtminer.txt", index=False)
    
    predicted_positive = candidate_gdas[candidate_gdas.predict==1][['pmid', 'geneId', 'diseaseId']].drop_duplicates()
    
    actual_positive = pd.read_csv('../../testing_data/labels.csv')
    
    true_positives = pd.merge(actual_positive, predicted_positive, how="inner")
    
    precision = true_positives.shape[0] / float(predicted_positive.shape[0])
    
    recall = true_positives.shape[0] / float(actual_positive.shape[0])
    
    Fscore = 2 * precision * recall /(precision + recall)
    
    print "precision:{}  recall:{}  F-score:{}".format(precision, recall, Fscore)
    
    subprocess.call(["rm", "out/feature.hybrid"])
    subprocess.call(["rm", "out/DTMiner_predict"])
    subprocess.call(["rm", "out/candidate_GDAs.csv"])