# RENET: A Deep Learning Approach for Extracting Gene-Disease Associations from Literature
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)  
Contact: Ye Wu  
Email: ywu@cs.hku.hk  
***

## Installation
```shell
git clone git@bitbucket.org:alexwuhkucs/gda-extraction.git
cd gda-extraction
```
## Quick Start after Installation
```shell
cd gda-extraction/testing_data
python ../renet/evaluate.py \
        --model_dir ../model/renet/ \
        --ab_fn abstracts.txt --stc_fn sentences.txt \
        --ner_fn anns.txt \
        --label_fn labels.csv \
        --output_fn classification_result.txt
```
***

## Introduction
Over one million new biomedical articles are published every year. Efficient and accurate text-mining tools are urgently needed to automatically extract knowledge from these articles to support research and genetic testing, in particular, articles on the extraction of gene-disease associations. However, existing text-mining tools for extracting gene-disease associations have limited capacity, as each sentence is considered separately. Our experiments show that the best existing tools, such as BeFree and DTMiner, achieve precision of 48% and recall rate of 78% at most. In this study, we designed and implemented a deep learning approach, named RENET, which considers the correlation between the sentences in an article to extract gene-disease associations. Our method has significantly improved the precision and recall rate to 85.2% and 81.8%, respectively. 

***

## Prerequisition
### Basics
RENET requires Python 2.7.  
Make sure you have Tensorflow ≥ 1.10.0 installed, the following commands install the lastest CPU version of Tensorflow

```shell
pip install tensorflow
pip install pandas
pip install numpy  
```

To check the version of Tensorflow you have installed:  

```shell
python -c 'import tensorflow as tf; print(tf.__version__)'
```
To do testing using trained models, CPU will suffice. To train a new model, a high-end GPU and the GPU version of Tensorflow is needed. To install the GPU version of tensorflow:
```shell
pip install tensorflow-gpu
```

## Quick Start


### Validate test results
```shell
python ./renet/evaluate.py \
        --model_dir ./model/renet/ \
        --ab_fn testing_data/abstracts.txt \
        --stc_fn testing_data/sentences.txt \
        --ner_fn testing_data/anns.txt \
        --label_fn testing_data/labels.csv \
        --output_fn testing_data/classification_result.txt
```
## Build a Model
```shell
python ./renet/train.py \
        --ab_fn training_data/abstracts.txt \
        --stc_fn training_data/sentences.txt \
        --ner_fn training_data/anns.txt \
        --label_fn training_data/labels.csv \
        --output_dir ../model/renet/
```
## Understand Output File
There are four columns in the outputfile classification_result.txt:  

1 | 2 | 3 | 4 | 
--- | --- | --- | --- | 
Article PubMed Id | Gene ID (Entrez) | Disease Id (MESH) | Predict 

The column Predict shows the predict result of RENET, 1 means RENET believes it is a true associaton, 0 otherwise.

## Benchmark
#### For BeFree [1]
```shell
pip install pymongo
pip install regex
cd benchmark/BeFree
git clone git@bitbucket.org:ibi_group/befree.git
sh benchmark_befree.sh
```
#### For DTMiner [2]
```shell
cd benchmark/DTMiner
sh benchmark_DTMiner.sh	
```
## Database for Gene-Disease-Article Associations
Please check gda_database/gdas.csv. Articles are extracted from PubMed query as follows.

“Psychiatry and Psychology Category” [Mesh] AND “genetics” [Subheading])
OR (“Diseases Category” [Mesh] AND “genetics” [Subheading]) AND (hasabstract[text] AND
(“1980” [PDAT]: “2018” [PDAT]) AND “humans” [MeSH Terms] AND English[lang]).

## Reference
[1] Bravo,À. et al. (2015) Extraction of relations between genes and diseases from text and large-scale data analysis: implications for translational research. BMC Bioinformatics, 16, 55.

[2] DTMiner: identification of potential disease targets through biomedical literature mining. 2016;32(23):3619-3626.
