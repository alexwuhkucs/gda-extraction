* We use MESH as identifiers for diseases and Entrez as identifiers for genes.
* Articles are extracted from PubMed query as follows.

        “Psychiatry and Psychology Category” [Mesh] AND “genetics” [Subheading])
        OR (“Diseases Category” [Mesh] AND “genetics” [Subheading]) AND (hasabstract[text] AND
        (“1980” [PDAT]: “2018” [PDAT]) AND “humans” [MeSH Terms] AND English[lang]).